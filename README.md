>  **WARNING:** incompatible with macOS Catalina and superior (MacOS >= 10.15).
> This software is no longer maintained. Operation requires an activation code provided by the IRCAM Forum Premium subscription.

After an analysis phase, the user modifies directly the result of the analysis in order to apply the desired changes to the sound. The main types of sound modification are: filtering, cross synthesis and dilation or compression of time and copy-paste of time-frequency zones.

AudioSculpt is powered by multiple engines : [SuperVP](https://forum.ircam.fr/projects/detail/supervp-for-max/) (Super Phase Vocoder), Pm2 (Partial Manager 2) and IrcamBeat (Ircam Beat detector). AudioSculpt can also script [SuperVP](https://forum.ircam.fr/projects/detail/supervp-for-max/) or Pm2 command line applications directly using its console view, wich allows to access powerfull DSP functions unreachable by the GUI.

![](https://forum.ircam.fr/media/uploads/Softwares/as3.png)

## Main Features ##
**Display:** Zoom from an overall view down to sample level, synchronized sonogram (time/frequency), spectral envelope, processing sequencer.

**Analysis:** FFT, LPC, True Envelope, fundamental frequency, partial following, different methods of automatic segmentation, formants, and tools for listening to selected sections of the spectra with harmonic display.

**Markers and Segmentation :** transient, beat, standard markers detection, import & export supported.

**Sound Processing:** A processing sequencer with individual tracks makes it possible to listen to them in real-time prior to creating the final result:

**Filtering:** directly on the sonogram using a pencil or eraser or by specifying with the mouse the points forming a polygon. Manual source separation and denoising are possible.

**Compression/expansion:** allows modification of the length of a sound without changing its pitch or timbre while maintaining the quality of the transitions.

**Transposition:** using a specific editor the user can transpose a sound without changing its length and without loosing sound quality ; separate transpositions for sound and spectral envelope for timbral modifications.

**Denoising:** spectral subtraction with interpolation of sound estimations.

**Cross Synthesis:** application of spectral data from one sound to another in order to create a hybrid sound or a transition from one sound to another.

**Partial Synthesis:** creation of a new sound from partials found via an analysis.

![](https://forum.ircam.fr/media/uploads/Softwares/audiosculpt-snapshot2.jpg)

## Design and Development ##
AudioSculpt, [SuperVP](https://forum.ircam.fr/projects/detail/supervp-for-max/), [Pm2](https://forum.ircam.fr/projects/detail/pm2/) and [IrcamBeat](http://www.quaero.org/module_technologique/ircambeat-music-tempo-meter-beat-and-downbeat-estimation/) are developed by the Analysis/Synthesis team at IRCAM.


>  **IMPORTANT NOTE:** 
>
>  The [Analysis/Synthesis Command-line Tools](https://forum.ircam.fr/projects/detail/analysissynthesis-command-line-tools/)
 (SuperVP & Pm2) are already included in AudioSculpt !
